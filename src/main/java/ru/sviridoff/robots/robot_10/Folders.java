package ru.sviridoff.robots.robot_10;

import java.util.HashMap;
import java.util.Map;

/**
 * @class Папки групп
 * @author Денис Свиридов
 * */
class Folders {

    Map<String, String> getFolders() {
        Map<String, String> folders = new HashMap<>();
        folders.put(Groups.G_ARCHIVE, __ARCHIVES);
        folders.put(Groups.G_BIN, __BINARIES);
        folders.put(Groups.G_DOCUMENTS, __DOCUMENTS);
        folders.put(Groups.G_ISO, __IMAGES);
        folders.put(Groups.G_IMAGES, __IMG);
        folders.put(Groups.G_SOURCES, __SOURCES);
        folders.put(Groups.G_TORRENTS, __TORRENTS);
        folders.put(Groups.G_FOLDERS, __FOLDERS);
        return folders;
    }

    private final static String __FOLDERS = "Папки";

    /**
     * @details Группа изображений
     * */
    private final static String __IMG = "Изображения";

    /**
     * @details Группа образов
     * */
    private final static String __IMAGES = "Образы";

    /**
     * @details Группа exe'шников
     * */
    private final static String __BINARIES = "Бинарники";


    /**
     * @details Группа архивов
     * */
    private final static String __ARCHIVES = "Архивы";

    /**
     * @details Группа документов
     * */
    private final static String __DOCUMENTS = "Документы";


    /**
     * @details Группа торрентов
     * */
    private final static String __TORRENTS = "Торренты";

    /**
     * @details Группа исходников
     * */
    private final static String __SOURCES = "Исходники";

}
