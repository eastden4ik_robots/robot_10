package ru.sviridoff.robots.robot_10;

/**
 * @class Группы типов
 * @author Денис Свиридов
 * */
class FilesTypes {

    /**
     * @details Группа изображений
     * */
    private static final String IMG_JPG = "jpg";
    private static final String IMG_PNG = "png";
    private static final String IMG_GIF = "gif";
    private static final String IMG_JPEG = "jpeg";
    private static final String IMG_BMP = "bmp";

    /**
     * @details Группа образов
     * */
    private static final String IMAGES_ISO = "iso";
    private static final String IMAGES_DMG = "dmg";
    private static final String IMAGES_OVA = "ova";
    private static final String IMAGES_OVBOX_EXTPACK = "vbox-extpack";

    /**
     * @details Группа exe'шников
     * */
    private static final String BIN_EXE = "exe";
    private static final String BIN_MSI = "msi";
    private static final String BIN_rmskin = "rmskin";
    private static final String BIN_DAT = "dat";
    private static final String BIN_JAR = "jar";
    private static final String BIN_APK = "apk";

    /**
     * @details Группа архивов
     * */
    private static final String ARCH_ZIP = "zip";
    private static final String ARCH_7Z = "7z";
    private static final String ARCH_RAR = "rar";
    private static final String ARCH_GZ = "gz";

    /**
     * @details Группа документов
     * */
    private static final String WND_DOC = "doc";
    private static final String WND_DOCX = "docx";
    private static final String WND_XLS = "xls";
    private static final String WND_XLSX = "xlsx";
    private static final String WND_PPT = "ppt";
    private static final String WND_PPTX = "pptx";
    private static final String WND_RTF = "rtf";
    private static final String WND_PDF = "pdf";
    private static final String WND_TXT = "txt";
    private static final String WND_HTML = "html";
    private static final String WND_HTM = "htm";
    private static final String WND_PCAP = "pcap";
    private static final String WND_WEBP = "webp";
    private static final String WND_CSV = "csv";
    private static final String WND_EPUB = "epub";

    /**
     * @details Группа торрентов
     * */
    private static final String TORRENT = "torrent";

    /**
     * @details Группа исходников
     * */
    private static final String SOURCE_PY = "py";
    private static final String SOURCE_IPYNB = "ipynb";
    private static final String SOURCE_JAVA = "java";
    private static final String SOURCE_CPP = "cpp";
    private static final String SOURCE_H = "h";
    private static final String SOURCE_VBS = "vbs";
    private static final String SOURCE_ASM = "asm";
    private static final String SOURCE_CONF = "conf";
    private static final String SOURCE_BAT = "bat";


    String classification(String type) {
        String res;
        switch (type.toLowerCase()) {
            case FilesTypes.ARCH_GZ:
            case FilesTypes.ARCH_ZIP:
            case FilesTypes.ARCH_RAR:
            case FilesTypes.ARCH_7Z : {
                res = Groups.G_ARCHIVE;
                break;
            }
            case FilesTypes.IMAGES_OVA:
            case FilesTypes.IMAGES_OVBOX_EXTPACK:
            case FilesTypes.IMAGES_DMG:
            case FilesTypes.IMAGES_ISO: {
                res = Groups.G_ISO;
                break;
            }
            case FilesTypes.BIN_APK:
            case FilesTypes.BIN_JAR:
            case FilesTypes.BIN_DAT:
            case FilesTypes.BIN_MSI:
            case FilesTypes.BIN_rmskin:
            case FilesTypes.BIN_EXE: {
                res = Groups.G_BIN;
                break;
            }
            case FilesTypes.TORRENT: {
                res = Groups.G_TORRENTS;
                break;
            }
            case FilesTypes.SOURCE_BAT:
            case FilesTypes.SOURCE_CONF:
            case FilesTypes.SOURCE_ASM:
            case FilesTypes.SOURCE_CPP:
            case FilesTypes.SOURCE_H:
            case FilesTypes.SOURCE_IPYNB:
            case FilesTypes.SOURCE_JAVA:
            case FilesTypes.SOURCE_VBS:
            case FilesTypes.SOURCE_PY: {
                res = Groups.G_SOURCES;
                break;
            }
            case FilesTypes.IMG_BMP:
            case FilesTypes.IMG_GIF:
            case FilesTypes.IMG_JPEG:
            case FilesTypes.IMG_JPG:
            case FilesTypes.IMG_PNG: {
                res = Groups.G_IMAGES;
                break;
            }
            case FilesTypes.WND_EPUB:
            case FilesTypes.WND_CSV:
            case FilesTypes.WND_HTM:
            case FilesTypes.WND_PCAP:
            case FilesTypes.WND_WEBP:
            case FilesTypes.WND_HTML:
            case FilesTypes.WND_DOC:
            case FilesTypes.WND_DOCX:
            case FilesTypes.WND_PDF:
            case FilesTypes.WND_PPT:
            case FilesTypes.WND_PPTX:
            case FilesTypes.WND_RTF:
            case FilesTypes.WND_TXT:
            case FilesTypes.WND_XLS:
            case FilesTypes.WND_XLSX: {
                res = Groups.G_DOCUMENTS;
                break;
            }
            default: {
                res = "Unknown file type: " + type;
            }
        }
        return res;
    }

    String getExt(String fileName) {
        return fileName.substring(fileName.lastIndexOf(".") + 1 );
    }

}
