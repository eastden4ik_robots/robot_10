package ru.sviridoff.robots.robot_10;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

class Utils {

    String getDate(String pattern) {
        Date now = new Date();
        DateFormat df = new SimpleDateFormat(pattern);
        return df.format(now);
    }

    void log(String path, String message) {
        File file = new File(path);
        try {
            FileWriter fw = new FileWriter(file, true);
            fw.write(getDate("[dd.MM.yyyy - HH:mm]: ") + message + "\n\r");
            fw.close();
        } catch (IOException ex) {
            ex.getMessage();
        }
    }

}
