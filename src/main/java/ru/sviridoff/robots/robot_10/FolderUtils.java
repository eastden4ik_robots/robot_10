package ru.sviridoff.robots.robot_10;

import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.util.Map;

@Slf4j
class FolderUtils {

    private static FilesTypes types = new FilesTypes();

    private String getResultPath (String drive, String folder, String destination) {
        return drive + File.separator + folder + File.separator + destination + File.separator;
    }

    void createFolders(Map<String, String> foldersNames) {

        foldersNames.forEach((type, name) -> {
            File file = new File(Config._DISK_DRIVE + File.separator + Config._MESS_FOLDER + File.separator + name);
            if (!file.exists()) {
                if (file.mkdir()) {
                    log.info("Каталог успешно создан: [{}].", getResultPath(Config._DISK_DRIVE, Config._MESS_FOLDER, name));
                }
            } else {
                log.info("Каталог уже имеется: [{}].", getResultPath(Config._DISK_DRIVE, Config._MESS_FOLDER, name));
            }
        });

    }

    private void moveFiles(Map<String, String> foldersNames, File file) throws IOException {
        if (!file.isDirectory()) {
            if (!types.classification(types.getExt(file.getName())).contains("Unknown file type")) {
                log.info("Перемещение файла " + file.getName() + " в папку " + foldersNames.get(types.classification(types.getExt(file.getName()))));
                if (file.exists()) {
                    Files.move(Paths.get(file.getAbsolutePath()) , Paths.get(getResultPath(Config._DISK_DRIVE, Config._MESS_FOLDER, foldersNames.get(types.classification(types.getExt(file.getName())))) + file.getName()), StandardCopyOption.REPLACE_EXISTING);
                }
            } else {
                log.error("Unknown file type: [" + file.getName() + "].");
            }
        } else if (!foldersNames.containsValue(file.getName())) {
            log.info("FOLDER: " + file.getName());
            Files.move(Paths.get(file.getAbsolutePath()) , Paths.get(getResultPath(Config._DISK_DRIVE, Config._MESS_FOLDER, foldersNames.get(Groups.G_FOLDERS)) + file.getName()), StandardCopyOption.REPLACE_EXISTING);

        }
    }

    void cleanFolder(Map<String, String> foldersNames) {

        try {
            String path = Config._DISK_DRIVE + File.separator + Config._MESS_FOLDER;
            File folder = new File(path);
            File[] files = folder.listFiles();
            if (files != null) {
                for (File file: files) {
                    moveFiles(foldersNames, file);
                }
            }
        } catch (IOException ex) {
            log.error(ex.getMessage());
        }

    }


}
