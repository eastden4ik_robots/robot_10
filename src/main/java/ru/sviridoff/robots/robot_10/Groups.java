package ru.sviridoff.robots.robot_10;

class Groups {

    final static String G_IMAGES = "img";
    final static String G_ISO = "images";
    final static String G_BIN = "binary";
    final static String G_ARCHIVE = "archive";
    final static String G_DOCUMENTS = "document";
    final static String G_SOURCES = "sources";
    final static String G_TORRENTS = "torrent";
    final static String G_FOLDERS = "folders";

}
