package ru.sviridoff.robots.robot_10;


import lombok.extern.slf4j.Slf4j;
import ru.sviridoff.framework.props.Parameters;

@Slf4j
public class Robot_10 {

    private static Folders folders = new Folders();
    private static FolderUtils folderUtils = new FolderUtils();
    private static Parameters parameters = new Parameters(System.getenv("robot.file"));

    public static void main(String[] args) {


        String drive = (String) parameters.getValue("drive");
        String folder = (String) parameters.getValue("folder");
        String title = (String) parameters.getValue("title");

        Config._DISK_DRIVE = drive;
        Config._MESS_FOLDER = folder;

        log.info("[{}] запущен.", title);
        log.info("Создании в директории [{}] набора директорий [{}].", drive + "\\" + folder, folders.getFolders().values());
        folderUtils.createFolders(folders.getFolders());
        log.info("Чистка директории [{}].", drive + "\\" + folder);
        folderUtils.cleanFolder(folders.getFolders());
        log.info("[{}] завершил работу.", title);

    }

}
